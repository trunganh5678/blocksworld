using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Block : MonoBehaviour
{
    public int ID;
    public Vector4[] colors;
    public SpriteRenderer sprite;

    public int colorIndex = 0;

    private void Start()
    {
        if (colors.Length != 0) colorIndex = ID % colors.Length;
        sprite = GetComponent<SpriteRenderer>();
        if (sprite == null) Debug.Log("no sprite");

        switch (colorIndex)
        {
            case 1: sprite.color = Color.red;
                break;
            case 2: sprite.color = Color.green;
                break;
            case 3: sprite.color = Color.blue;
                break;
            default: sprite.color = Color.white;
                break;
        }
    }
}
